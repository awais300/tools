<?php
/*
--------------------------------------------------------------------------------------------------------
Archive Usage:
http://example.com/create-archive.php?backup_path={/path/to/dir}&save_file_name={devzone_sample.zip}
Archive usage predict path itself:
http://example.com/create-archive.php?backup_path&save_file_name={devzone_sample.zip}
Unarchive Usage:
http://example.com/create-archive.php?unzip_file={devzone_sample.zip}
--------------------------------------------------------------------------------------------------------
*/


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

define('BR', '<br/>');
set_time_limit(0);
ini_set('memory_limit', '-1');

//echo output
ob_implicit_flush();
ob_end_flush();

/**
 * create a zip archive file
 * @param  string  $source
 * @param  string  $destination
 * @param  boolean $include_dir
 * @param  array   $ignore_files
 * @param  string   $ignore_folders
 */
function create_archive($source, $destination, $include_dir = false,
    $ignore_files = array(), $ignore_extensions = array(), $ignore_folders = '') {
    //Ignore '.' and '..' files
    $explict_ignore_files = array('.', '..');

    //include more files to ignore
    $all_ignore_files = array_merge($explict_ignore_files, $ignore_files);

    if (!extension_loaded('zip') || !file_exists($source)) {
        exit('PHP ZIP xtension is not loaded or File path is wrong!');
    }

    if (file_exists($destination)) {
        unlink($destination);
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        exit('Could not start the ZipArchive, appears destination file path is wrong!');
        return false;
    }
    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true) {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source),
            RecursiveIteratorIterator::SELF_FIRST);
        if ($include_dir) {
            $array   = explode('/', $source);
            $maindir = $array[count($array) - 1];

            $source = '';
            for ($i = 0; $i < count($array) - 1; $i++) {
                $source .= '/' . $array[$i];
            }

            $source = substr($source, 1);
            $zip->addEmptyDir($maindir);
        }

        foreach ($files as $file) {
            $file = str_replace('\\', '/', $file);



            // purposely ignore files that are irrelevant
            if (in_array(substr($file, strrpos($file, '/') + 1), $all_ignore_files)) {
                continue;
            }
            if (in_array(strtolower(pathinfo($file, PATHINFO_EXTENSION)), $ignore_extensions)) {
                continue;
            }
            if(!empty($ignore_folders) && (preg_match("/$ignore_folders/i", $file, $output_array) == 1)) {
                    continue;
            }

            $file = realpath($file);

            if (is_dir($file) === true) {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            } else if (is_file($file) === true) {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    } else if (is_file($source) === true) {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}

/**
 * force download a file
 * @param  string $file
 * @return void
 */
function download_file($file)
{
    //Check file exist or not
    if (file_exists($file)) {
        if (ini_get('zlib.output_compression')) {
            //IE hack
            ini_set('zlib.output_compression', 'Off');
        }

        // Get mine type of file.
        $finfo    = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
        $mimeType = finfo_file($finfo, $file) . "\n";
        finfo_close($finfo);
        header('Expires: 0');
        header('Pragma: public');
        header('Cache-Control: private', false);
        header('Content-Type:' . $mimeType);
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Length: ' . filesize($file));
        header('Connection: close');
        readfile($file);
        exit;
    } else {
        echo 'File does not exist' . BR;
    }
}

/**
 * unzip an archive
 * @param  string $file
 * @return void
 */
function unzip_archive($file) {
    //Get the absolute path to $file
    $path = pathinfo(realpath($file), PATHINFO_DIRNAME);
    $zip = new ZipArchive;
    $is_open = $zip->open($file);
    if ($is_open === TRUE) {
      // extract it to the path we determined above
      $result = $zip->extractTo($path);
      if(!$result) {
        exit('There is problem while extracting archive');
      }
      return $zip->close();
    } else {
      exit('Could not open file to extract');
    }
}

/**
 * get the site URL
 * @return string
 */
function get_url()
{
    $url = sprintf(
        "%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        $_SERVER['REQUEST_URI']
    );

    return rtrim($url, '/');
}

//trigger logic
if (isset($_GET['backup_path']) && isset($_GET['save_file_name'])) {
    $path           = $_GET['backup_path'];
    if(empty($path)) {
        $path =  __dir__;
    }

    $save_file_name = $_GET['save_file_name'];
    $save_file_name = trim(date('m-d-Y_H-i-s') . '_' . $save_file_name);

    $ignore_files_array = array(
        'DS_Store',
    );
    $ignore_extensions = array(
        'zip',
        'tar',
        'gz',
        'bz2',
        'rar'
    );
    $ignore_folders = array(
    );

    $ignore_folders = implode('|', $ignore_folders);

    echo 'Starting scan...' . BR;
    if (create_archive($path, $save_file_name, false, $ignore_files_array, $ignore_extensions, $ignore_folders)) {
        echo 'Scan finished' . BR;
        echo "<a href='{$save_file_name}?force_download=yes download='{$save_file_name}'>Download Backup File</a>";
    } else {
        exit('Could not create zip archive' . BR);
    }
} else if (isset($_GET['force_download']) && $_GET['force_download'] == 'yes') {
    //download_file(); //php way to force download
} else if(isset($_GET['unzip_file']) && !empty($_GET['unzip_file'])) {
    if(unzip_archive($_GET['unzip_file'])) {
        exit('Zip archive extracted successfully' . BR);
    } else {
        exit('Something went wrong while extracting archive' . BR);
    }
} else {
    exit('Required parameter is missing.' . BR);
}