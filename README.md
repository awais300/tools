# README #

# create-archive.php #
### Usage:###

* Create archive by path  
http://example.com/create-archive.php?backup_path={/path/to/dir}&save_file_name={devzone_sample.zip}
* Create archive, predict path itself  
http://example.com/create-archive.php?backup_path&save_file_name={devzone_sample.zip}
* Unarchive  
http://example.com/create-archive.php?unzip_file={devzone_sample.zip}

# create-dbbackup.php #
### Usage: ###
* create a database backup  
http://example.com/create-dbbackup.php?&mode=backup&db_name={village}&db_user={root}&db_pass={mysql}&db_host={default_is_localhost}
* Restore database  
http://example.com/create-dbbackup.php?mode=restore&restore_file={file.sql}&db_name={village}&db_user={root}&db_pass={mysql}&db_host={default_is_localhost}
* Restore database advance  
http://example.com/create-dbbackup.php?mode=adv_restore&restore_file={file.sql}&db_name={village}&db_user={root}&db_pass={mysql}&db_host={default_is_localhost}

# create-vhost #
### Usage: ###  
* Bash script to create virtual host and adds entry in /etc/hosts as well to setup a quick local web environment in seconds  
$ sudo ./create-vhost {example.dev}

# create-wp-user.php #
### Usage: ###
* Create a wordpress admin user  
http://example.com/create-wp-user.php