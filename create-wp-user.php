<?php 
session_start();
$break = '<br />';

if(isset($_GET['csv']) == 'ok') {
	$user_array = $_SESSION['user_info'];
	if(empty($user_array)){
		echo 'CSV exported already!';
		exit(0);
	}

	//download in csv format
	create_csv($user_array);

	//destroy session
	$user_array = $_SESSION['user_info'] = null;
	session_destroy();
}
else {
	require_once('wp-load.php');
	$user_role = null;
	$user_flag = false;

	$user_name = 'hidevzone';
	$user_email = 'devlopmentzone@gmail.com';

	$user_id = username_exists($user_name);
	if ( !$user_id and email_exists($user_email) == false ) {
		//use wp function to generate password and create user
		$random_password = wp_generate_password($length=16, $include_standard_special_chars = false);
		$user_id = wp_create_user( $user_name, $random_password, $user_email );

		$user = get_user_by('id', $user_id);
		$role =  get_userdata($user_id);
		$role = $role->roles[0];
		if($role != 'administrator') {
			$user->remove_role($role);
			$user->add_role('administrator');
		}
	} else {
		$user_flag = true;
		$random_password = __('User already exists! Use your old password or reset your password.');
	}

	echo 'Username: ' . $user_name . $break;
	echo 'Password: ' . $random_password . $break;

	if(!$user_flag) {
		$user_array = array('HiDevZone User Info', 
			'http://hidevzone.com', 
			$user_name, 
			$random_password, 
			$user_email,
			'HiDevZone admin username and password information'
		);
		$_SESSION['user_info'] = $user_array;
		echo '<a href="create-wp-user.php?csv=ok">Download CSV</a>';
	}
}

function create_csv($user_array) {
	header('Content-Encoding: UTF-8');
	header("Content-type: text/csv; charset=UTF-8");
	header("Content-Disposition: attachment; filename=1password.csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo "\xEF\xBB\xBF";
	$fp = fopen('php://output', 'w');
	$list = array(
	        array('Title', 'URL', 'User Name', 'Password', 'Email', 'info'),
	        $user_array
	    );
	foreach ($list as $fields) {
	    fputcsv($fp, $fields);
	}
	fclose($fp);
}